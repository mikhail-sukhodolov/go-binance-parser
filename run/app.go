package run

import (
	"binance-parser/config"
	"binance-parser/internal/handlers"
	"binance-parser/internal/models"
	"binance-parser/internal/service"
	"binance-parser/internal/storage"
	"binance-parser/internal/worker"
	"context"
	"gitlab.com/golight/boilerplate/infrastructure/errors"
	"gitlab.com/golight/boilerplate/infrastructure/server"
	"gitlab.com/golight/boilerplate/utils"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

type App struct {
	conf    config.AppConf
	logger  *zap.Logger
	Sig     chan os.Signal
	srv     server.Server
	Storage *storage.Storage
	Service *service.Service
	Handler *handler.Handler
	Worker  *worker.Worker
}

// конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

func (a *App) Run() int {
	// группа для обработки ошибок в горутинах
	errGroup, ctxWithCancel := errgroup.WithContext(context.Background())
	_, cancel := context.WithCancel(ctxWithCancel)

	// запускаем горутину для graceful shutdown
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt received", zap.Stringer("os_signal", sigInt))
		cancel() // Отправляем сигнал для завершения всех горутин
		return ctxWithCancel.Err()
	})
	// запускаем http сервер
	errGroup.Go(func() error {
		if err := a.srv.Serve(ctxWithCancel); err != nil {
			a.logger.Error("HTTP server error", zap.Error(err))
			cancel() // Отправляем сигнал для завершения всех горутин
			return err
		}
		return nil
	})
	// проверяем, завершились ли все горутины без ошибок, если какая-то горутина возвращает ошибку
	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	// проверяем, завершились ли все горутины без ошибок
	return errors.NoError
}

func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация сканера таблиц
	tableScanner := utils.NewTableScanner()

	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.KLineDTO{},
	)
	// инициализация базы данных sql и адаптера
	sqlAdapter, err := utils.NewOrm(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("init db error")
	}
	//инициализация хранилища
	a.Storage = storage.NewStorage(sqlAdapter, a.logger)

	// инициализация воркера
	a.Worker = worker.NewWorker(a.logger, a.Storage, a.conf.WorkerApi)

	//инициализация сервиса
	a.Service = service.NewService(a.Storage, a.Worker)

	// инициализация хендлера
	a.Handler = handler.NewHandler(a.Service, a.logger)

	// инициализация http сервера
	httpServer := &http.Server{Addr: ":8080", Handler: a.Handler.InitRoutes()}
	a.srv = server.NewHttpServer(a.conf.Server, httpServer, a.logger)

	return a
}
