package models

import (
	"time"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index
type KLineDTO struct {
	ID                  int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name                string    `json:"name" db:"name" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
	OpenTime            time.Time `json:"open_time" db:"open_time" db_type:"timestamp" db_default:"default now()" db_ops:"create,update"`
	Open                float64   `json:"open" db:"open" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	High                float64   `json:"high" db:"high" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	Low                 float64   `json:"low" db:"low" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	Close               float64   `json:"close" db:"close" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	Volume              float64   `json:"volume" db:"volume" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	CloseTime           time.Time `json:"close_time" db:"close_time" db_type:"timestamp" db_default:"default now()" db_ops:"create,update"`
	QuoteVolume         float64   `json:"quote_volume" db:"quote_volume" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	Count               int       `json:"count" db:"count" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	TakerBuyVolume      float64   `json:"taker_buy_volume" db:"taker_buy_volume" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	TakerBuyQuoteVolume float64   `json:"taker_buy_quote_volume" db:"taker_buy_quote_volume" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	Ignore              string    `json:"ignore" db:"ignore" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
}

func (K *KLineDTO) TableName() string {
	return "kline"
}

func (K KLineDTO) OnCreate() []string {
	return []string{}
}

type KLines []KLine

type KLine struct {
	Name                string    `json:"name"`
	OpenTime            time.Time `json:"open_time"`              // Время открытия K-line в формате времени Unix
	Open                float64   `json:"open"`                   // Цена открытия
	High                float64   `json:"high"`                   // Макс. цена
	Low                 float64   `json:"low"`                    // Мин. цена
	Close               float64   `json:"close"`                  // Цена закрытия
	Volume              float64   `json:"volume"`                 // Объем
	CloseTime           time.Time `json:"close_time"`             // Время закрытия K-line в формате времени Unix
	QuoteVolume         float64   `json:"quote_volume"`           // Объем котируемых активов
	Count               int       `json:"count"`                  // Количество сделок
	TakerBuyVolume      float64   `json:"taker_buy_volume"`       // Объем базовых активов тейкера в течение этого периода
	TakerBuyQuoteVolume float64   `json:"taker_buy_quote_volume"` // Объем котируемых активов тейкера в течение этого периода
	Ignore              string    `json:"ignore"`                 // Игнорировать
}

type RequestData struct {
	Symbol string `json:"symbol"`
}
