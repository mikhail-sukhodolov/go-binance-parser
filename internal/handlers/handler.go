package handler

import (
	"binance-parser/internal/models"
	"binance-parser/internal/service"
	"context"
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
)

type Handler struct {
	Service service.Servicer
	logger  *zap.Logger
}

func NewHandler(service service.Servicer, logger *zap.Logger) *Handler {
	return &Handler{Service: service, logger: logger}
}

// StartParsing
// @Summary      StartParsing
// @Description  Parsing k-line pairs
// @Tags         parser
// @Success      200
// @Router       /start [post]
func (h *Handler) StartParsing(w http.ResponseWriter, r *http.Request) {
	h.Service.StartParsing(context.Background())
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Parsing started"))
}

// StopParsing
// @Summary      StopParsing
// @Description  Stop parsing k-line pairs
// @Tags         parser
// @Success      200
// @Router       /stop [post]
func (h *Handler) StopParsing(w http.ResponseWriter, r *http.Request) {
	h.Service.StopParsing()
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Parsing stoped"))
}

// GetInfo
// @Summary      GetInfo
// @Description  get pair info
// @Tags         parser
// @Accept       json
// @Produce      json
// @Param   input   body   models.RequestData  true   "JSON object"
// @Failure      500
// @Success      200 {object} []models.KLineDTO
// @Router       /getinfo [get]
func (h *Handler) GetInfo(w http.ResponseWriter, r *http.Request) {
	request := models.RequestData{}

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Error("symbol decode error", zap.Error(err))
		return
	}

	klines, err := h.Service.GetInfo(request.Symbol)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.logger.Error("getinfo error", zap.Error(err))
		return
	}

	if err = json.NewEncoder(w).Encode(klines); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.logger.Error("encode error", zap.Error(err))
		return
	}
}
