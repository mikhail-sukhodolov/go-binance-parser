package handler

import (
	_ "binance-parser/docs"
	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
)

func (h *Handler) InitRoutes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/swagger/*", httpSwagger.WrapHandler)

	r.Post("/start", h.StartParsing)
	r.Post("/stop", h.StopParsing)
	r.Get("/getinfo", h.GetInfo)
	return r
}
