package storage

import (
	"binance-parser/internal/models"
	"context"
)

type Storager interface {
	Save(ctx context.Context, klines []models.KLine) error
	GetInfo(symbol string) ([]models.KLineDTO, error)
}
